;
; Copyright (C) 2008  Alexander Potashev
;
; Calculating of the number of zero words of 10 top words in the stack
;

.model small
.code

start:
	push 0
	push 20
	push 30
	push 40
	push 50
	push 60
	push 70
	push 0
	push 90
	push 0

;-----------------------
	mov ax, dgroup
	mov ds, ax

;-----------------------
	mov bp, sp	; using "bp" to access stack variables
	mov ax, 0

	mov cx, 10
next_word:

	cmp word ptr [bp], 0	; educational way
	jne not_zero		;
	inc ax			;
not_zero:			;

;	mov dx, [bp]		; --- another, "cheat" way (without "cmp") ---
;	sub dx, 1		; CF=1 if dx=0
;	adc ax, 0		;

	add bp, 2
	loop next_word

;-----------------------
; now "ax" contains the result (possible values: 0..10)
	lea dx, text

	cmp ax, 10
	je result_10

	inc dx
	add al, '0'
	mov text[1], al

result_10:

	mov ah, 09h
	int 21h

;-----------------------

	add sp, 20

	mov ax, 4c00h
	int 21h

.data
text	db	"10", 13, 10, '$'

.stack 256

end start

