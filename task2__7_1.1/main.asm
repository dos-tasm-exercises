;
; Copyright (C) 2008  Alexander Potashev
;
; Calculating the number of 1's in the binary representation
;
; Better solutions:
; 1) Using new Intel processors instruction "POPCNT"
;	http://softwarecommunity.intel.com/isn/Downloads/Intel SSE4 Programming Reference.pdf
;		POPCNT - Return the Count of Number of Bits Set to 1
;	http://en.wikipedia.org/wiki/SSE4
;	http://ru.wikipedia.org/wiki/SSE4
;
; 2) Hackish algorithms
;	http://infolab.stanford.edu/~manku/bitcount/bitcount.html
;	http://en.wikipedia.org/wiki/Hamming_weight

.model small
.code

start:
	mov ax, 0cc12h

;-----------------------
	mov dx, dgroup
	mov ds, dx

;-----------------------
	mov dh, 0

	mov cx, 16		; simple algorithm
next_bit:
	mov bl, al
	and bl, 1
	add dh, bl

	shr ax, 1
	loop next_bit
;-----------------------

	mov al, dh		; the result

	lea dx, text+1
	cmp al, 10
	jl single_digit

	dec dx
	sub al, 10

single_digit:
	add al, '0'
	mov text[1], al

	mov ah, 09h
	int 21h

;-----------------------

	mov ax, 4c00h
	int 21h

.data
text	db	"1-", 13, 10, '$'

.stack 256

end start

