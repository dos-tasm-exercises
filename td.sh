#/bin/sh

#
# Copyright (C) 2008  Alexander Potashev
#

OLD_PWD=$PWD
TASM_DIR=../tasm-bin

cd $TASM_DIR
dosbox -exit td.bat
cd $OLD_PWD

