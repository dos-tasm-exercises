;
; Copyright (C) 2008  Alexander Potashev
;
; Deleting zero words from stack
;

.model small
.code

start:
	push 0		; [bp + 18]
	push 20
	push 30
	push 40
	push 50
	push 60
	push 70
	push 0
	push 90
	push 0

;-----------------------
	mov bp, sp	; using "bp" to access stack variables

	mov si, 18	; "si" - index of the current word
	mov di, si	; "di" - next index to write to

	mov cx, 10
next_word:

	cmp word ptr [bp + si], 0
	je is_zero

	mov ax, [bp + si]
	mov [bp + di], ax
	sub di, 2

is_zero:
	sub si, 2
	loop next_word


	add sp, di	; adjust the top of the stack ("sp")
	add sp, 2
;-----------------------

; we won't delete the nonzero words from stack...

	mov ax, 4c00h
	int 21h

.data
text	db	"10", 13, 10, '$'

.stack 256

end start

