;
; Copyright (C) 2008  Alexander Potashev
;
; Zeroing bytes by addresses
;

.model small
.code

start:
	push offset my_byte	; [bp + 18]
	push offset my_byte
	push offset my_byte
	push offset my_byte
	push offset my_byte
	push offset my_byte
	push offset my_byte
	push offset my_byte
	push offset my_byte
	push offset my_byte
	
;-----------------------
	mov ax, dgroup
	mov ds, ax

;-----------------------
	mov bp, sp	; using "bp" to access stack variables

	mov cx, 10
next_addr:
	mov bx, [bp]
	mov byte ptr [bx], 0

	add bp, 2
	loop next_addr
;-----------------------

	add sp, 20

	mov ax, 4c00h
	int 21h

.data
my_byte	db	'b'

.stack 256

end start

