;
; Copyright (C) 2008  Alexander Potashev
;
; Exchange of 2 top words in the stack, all registers saved
;

.model small
.code

start:
	push 10
	push 20
	push 30
	push 40
	push 50
	push 60
	push 70
	push 80
	push 90		; [bp + 6]
	push 100	; [bp + 4]

;-----------------------
	push ax		; save all registers which will be used
	push bp

	mov bp, sp	; using "bp" to access stack variables

	mov ax, [bp + 4]	; exchange
	xchg ax, [bp + 6]
	mov [bp + 4], ax

	pop bp		; restore registers
	pop ax
;-----------------------

	add sp, 20

	mov ax, 4c00h
	int 21h

.stack 256

end start

