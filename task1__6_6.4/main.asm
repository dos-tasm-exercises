;
; Copyright (C) 2008  Alexander Potashev
;
; Search for equal words (Are there any identical words in the stack?[yes/no])
;

.model small
.code

start:
	push 1		; [bp + 18]
	push 20
	push 30
	push 40
	push 50
	push 60
	push 1
	push 80
	push 90
	push 100

;-----------------------
	mov ax, dgroup
	mov ds, ax

;-----------------------
	mov bp, sp	; using "bp" to access stack variables

	mov si, 18	; "si" - index of the first word to compare
	mov cx, 9
first:
	push cx

	mov ax, [bp + si]	; ax = first word

	mov cx, si
	shr cx, 1	; cx = si/2
	lea di, [si-2]	; di = si-2
second:
	cmp ax, [bp + di]
	je yes

	sub di, 2
	loop second

	pop cx
	sub si, 2
	loop first

;-----------------------
	lea dx, text_no
	jmp no

yes:	lea dx, text_yes
no:
	mov ah, 09h
	int 21h

;-----------------------

	add sp, 20

	mov ax, 4c00h
	int 21h

.data
text_yes	db	"yes", 13, 10, '$'
text_no		db	"no", 13, 10, '$'

.stack 256

end start

