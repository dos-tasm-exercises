#!/bin/sh

#
# Copyright (C) 2008  Alexander Potashev
#

OLD_PWD=$PWD
TASM_DIR=../tasm-bin

cp main.asm $TASM_DIR

cd $TASM_DIR
dosbox -exit combuild.bat
cd $OLD_PWD


cd $TASM_DIR
dosbox -exit run.bat
cd $OLD_PWD

