;
; Copyright (C) 2008  Alexander Potashev
;

.model small
.code

start:
	mov ax, dgroup
	mov ds, ax

	mov ah, 09h
	lea dx, text
	int 21h

	mov ax, 4c00h
	int 21h


.data
text	db	"Hello, world!", 13, 10, '$'

.stack 100
end start

