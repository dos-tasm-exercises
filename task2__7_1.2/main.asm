;
; Copyright (C) 2008  Alexander Potashev
;
; Printing AX in binary notation
;

.model small
.code

start:
	mov ax, 4c12h

;-----------------------
	mov dx, dgroup
	mov ds, dx

;-----------------------
	mov bx, 0		; SI incrementation
	lea si, text

	mov cx, 16
next_bit:
	shl ax, 1		; moves the 15th bit to CF (Carry Flag)
	jc bit_1

	mov byte ptr [si], '0'	; 0

	jmp cont
bit_1:
	mov bx, 1		; 1
	mov byte ptr [si], '1'

cont:
	add si, bx
	loop next_bit
;-----------------------

	lea dx, text
	mov ah, 09h
	int 21h

;-----------------------

	mov ax, 4c00h
	int 21h

.data
text	db	17 dup ('$')

.stack 256

end start

